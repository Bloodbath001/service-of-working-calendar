
# Сервис календаря
---
```
1.этот код построен на основе микроархитектуре CRUD
```
1.create
2.read
3.upload
4.delete
## Краткое описание по запуску нашего api сервиса Календаря

Нужно активировать наше окружение,что бы мы могли провести тест в терминале,активировать окружение можно с помощью команды :
```
./venv/bin/flask --app ./Service of working Calendar/server.py run
```


## cURL тестирование

### добавление нового события
```
curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "date|title|text"
```

### получение всего списка календаря
```
curl http://127.0.0.1:5000/api/v1/calendar/
```

### получение события по идентификатору / ID == 1
```
curl http://127.0.0.1:5000/api/v1/calendar/1/
```

### обновление события календаря по идентификатору / ID == 1 /  новый текст == "new text"
```
curl http://127.0.0.1:5000/api/v1/calendar/1/ -X PUT -d "date|title|new text"
```

### удаление события по идентификатору / ID == 1
```
curl http://127.0.0.1:5000/api/v1/calendar/1/ -X DELETE
```


## тесты в консоли исполнения с выводом

```
curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "id<>|date|title|text"
new id: 1

curl http://127.0.0.1:5000/api/v1/calendar/
1|date|title|text

curl http://127.0.0.1:5000/api/v1/calendar/1/
1|date|title|text


curl http://127.0.0.1:5000/api/v1/calendar/1/ -X PUT -d "date|title|new text"
updated

curl http://127.0.0.1:5000/api/v1/calendar/1/
1|date|title|new text


###limit text,title and date
```
 curl http://127.0.0.1:5000/api/v1/calendar/1/ -X PUT -d "date|title|loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong text"
```

```
curl http://127.0.0.1:5000/api/v1/calendar/1/ -X PUT -d 
"date|loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong title|text"
```

```
curl http://127.0.0.1:5000/api/v1/calendar/1/ -X PUT -d "2020-12-23|title|text"
```


```
curl http://127.0.0.1:5000/api/v1/calendar/1/ -X DELETE
```

