# Делаем хранение данных
from typing import List


import model



class StorageExexeption(Exception):
    pass

class LocalStorage:
    def __init__(self):
        self._id_counter = 0
        self._storage = {}


    def create(self, calendar:model.calendar) -> str:
        self._id_counter = +1
        calendar.id = str(self._id_counter)
        self._storage[calendar] = calendar
        return calendar.id

    def list(self) -> List[model.calendar]:
        return list(self._storage.values())


    def read(self , _id:str ,calendar:model.calendar):
        if _id not in self._storage:
            raise StorageExexeption(F"{_id} if not found in storage")
        return self._storage[_id]


    def update(self , _id:str ,calendar:model.calendar):
        if _id not in self._storage:
            raise StorageExexeption(F"{_id} if not found in storage")
        calendar.id = _id
        self._storage[calendar.id] =calendar



    def delete(self , _id:str ,calendar:model.calendar):
        if _id not in self._storage:
            raise StorageExexeption(F"{_id} if not found in storage")
        del self._storage[_id]



