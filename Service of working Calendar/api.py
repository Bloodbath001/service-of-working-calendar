from flask import Flask
from flask import request

app = Flask(__name__)

import model
import storage


_storage = storage.LocalStorage()

#
class ApiEXEPTION(Exception):
    pass

# делаем разделитель или же функцую конвектора
# конвертацию добавлять только в апи,потому что оно касается
# только нашего интерфейса
def _from_raw(raw_calendar:str) -> model.calendar:
    parts = raw_calendar.split('|')
    if len(parts) == 2:
        # id пределяем как None,все остальное последовательно
        calendar = model.calendar()
        calendar.id = None
        calendar.title = str(parts[1])
        calendar.text = str(parts[2])
        calendar.date = str(parts[3])
        return calendar
    elif len(parts) == 3:
        # сделать id определяем как [0] все остальное оставляем так же
        calendar = model.calendar()
        calendar.id =str(parts[0])
        calendar.title =str(parts[1])
        calendar.text =str(parts[2])
        calendar.date =str(parts[3])
        return calendar
    else:
        raise ApiEXEPTION(f"invalid Raw note date {raw_calendar}")

# Часть бизнес логики
def _to_rate(calendar :model.calendar) -> str:
    if calendar.id is None:
        return f"{calendar.title}|{calendar.text}|{calendar.date}"
    else:
        return f"{calendar.id}|{calendar.title}|{calendar.text}|{calendar.date}"



API_ROOT = '/api/v1'

CALENDAR_API_ROOT =API_ROOT + '/calendar'

#cоздаем API интерфейс CRUD —
# Добавление / Список / Чтение / Обновление / Удаление

#декоратор на создание
@app.route(CALENDAR_API_ROOT + "/", methods=['POST'])
def create():
    data = str(request.get_data())
    calendar = _from_raw(data)
    return f"create :{calendar.id} / {calendar.title} /{calendar.text}/{calendar.date}", 201


#декоратор на список
@app.route(CALENDAR_API_ROOT + "/", methods=['Get'])
def list():
    return "list" ,200


# декоратор на чтение
@app.route(CALENDAR_API_ROOT + "/<_id>/", methods=['Get'])
def read(_id: str):
    calendar = model.calendar()
    calendar.id = 1
    calendar.title = "title_1"
    calendar.text = "text_2"
    calendar.date ="2024-03-17"
    # переводим в сырой формат данных
    raw_calendar = _to_rate(calendar)
    return raw_calendar ,200

# декоратор на обновление
@app.route(CALENDAR_API_ROOT + "/<_id>/", methods=['PUT'])
def update(_id: str):
    return "update" ,200


# декоратор на удаленние
@app.route(CALENDAR_API_ROOT + "/(_id>/", methods=['DELETE'])
def delete(_id: str):
    return "delete" ,200



