# Прописываем логику приложения,ограничение
from typing import List

import model


TITLE_LIMIT = 30
TEXT_LIMIT = 200
DATA_LIMIT = 1


class LogicException(Exception):
    pass



#делаем ограниченния на оглавление,текс и дату
@staticmethod
def _validate_calendar(calendar: model.calendar):
    if calendar is None:
        raise LogicException("calendar is None")
    if calendar.title is None or len(calendar.title) > TITLE_LIMIT:
        raise LogicException(f"title is MAX > {TITLE_LIMIT}")
    if calendar.text is None or len(calendar.text) > TEXT_LIMIT:
        raise LogicException(f"title is MAX > {TEXT_LIMIT}")
    if calendar.date is None or len(calendar.date) > DATA_LIMIT:
        raise LogicException(f"title is MAX > {DATA_LIMIT}")

